### What is this repository for? ###

This repositroy contains scripts to be used with vivado to produce the files for the fpga region generation. 
They extract information about modules from vivado that help to find possible region placements.

### How do I get set up? ###

These scripts are to be run with a synthesized design in vivado 2017.2. You need to first create text files
contain information for either utilization extraction or netlist extraction.

modules.txt -> netlist extraction
modules_util.txt -> utilization extraction

