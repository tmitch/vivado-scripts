#Need save the initial hierarchy of the design
report_utilization -hierarchical -file "lines.txt"


set fp [open "lines.txt" r]
set file_data [read $fp]

close $fp

#Extract module names stored in text file
set fp [open "modules.txt" r]
set names_data [read $fp]
close $fp

set hierarchical [open "hierarchical.txt" w]
set data [split $file_data "\n"]
set names [split $names_data "\n"]
foreach name $names {
    set parts [split $name "/"]
    set index 0
   # puts $parts
    foreach line $data {
        set s "* "
        append s [lindex $parts $index]
        append s " *"
        if [string match $s $line] {
            incr index
            if { $index == [llength $parts] } {
                puts $hierarchical $line
            } 
        }
    }
}


#foreach line $data {
     # do some line processing here
#    foreach d $names {
#        set s "* " 
#        set t "*("
#        append s $d 
#        append t "*"
#        append s " |*"
#        append t ")*"
#        if [string match $t $line] {
	        #Dummy as there is a second line that matches for each module that is not correct
#            set a " "
#	    } elseif [string match $s $line] {
            #Save the resources for that module
#            puts $hierarchical $line
 #       }
#	}
#}
close $hierarchical

#From the saved file, extract lines based of | as these are used to separate each values
set modules [open "hierarchical.txt" r]
set module_data [read $modules]
#To save results to
set utilization [open "utilization.txt" w]

set data [split $module_data "|"]
set i 0

set s ""
#For each module this extracts information based on what resrouce types are wanted
set index 0
foreach line $data {
    if {$i == 1} {
    	append s [lindex $names $index]
    }
    if {$i == 3} {
        set l [string trim $line]
        set s1 [expr $l]
        #Rounding the number of slices up
        if { ($s1%4) != 0} {
        	set s1 [expr $s1+4]
        }
    }
    if {$i == 7} {
        set l [string trim $line]
        set s2 [expr $l]
        #Rounding the number of ff up
        if { ($s2%8) != 0} {
        	set s2 [expr $s2+8]
        }
        set slice [expr max($s1/4,$s2/8)]
        append s " "
        append s $slice
    }
    if {$i == 8} {
    	#This has BRAM36
        set l [string trim $line]
        set bram36 [expr $l]
    }

    if {$i == 9} {
        #This deals with BRAM18
        #add one so total bram is rounded up
        set l [string trim $line]
        set bram18 [expr $l]
        #have to divide BRAM18 by 2
        set bram36 [expr ($bram36 * 2)]
        set bram [expr ($bram18 + $bram36)]

        append s " "
        append s $bram
    }

    if {$i == 10} {
        #This value is the number of DSP48 blocks
        append s " "
        append s [string trim $line]
    }

    incr i
    if {$i == 11} {
    	puts $utilization $s
    	set i 0
        set s ""
        incr index
    }
}


close $modules
close $utilization

#Delete the lines and hierarchical files
#file delete -force "lines.txt"
#file delete -force "hierarchical.txt"
