# Create a checkpoint of the synthesized DUT in order to not syntesize it in each iteration
proc normalFloorPlan {} {
    
    #Open synth design
    #open_run synth_1

    #copy module name file for resource extraction
    set copy [exec cp /home/tmitchell/Documents/tcl_scripts/modules.txt .]

    #Resource utilization Extraction
    source /home/tmitchell/Documents/tcl_scripts/utilization_2017.tcl

    #Netlist extraction
    if [file exists "connections.txt"] {
        #Don't need to create a connection document
    } else {
        #Create a connection document
        source /home/tmitchell/Documents/tcl_scripts/netsNew.tcl
    }

    #close_design

    #### REGION GENERATOR SECTION ####
    #### set microblaze location columns ####
    #### These values need to be set correctly ####
    set startX 0
    set startY 2
    set endX 35
    set endY 2

    # Run java file
    set reg_gen [exec java -cp /home/tmitchell/Documents/fpga_region_gen/ region_generator $startX $endX $startY $endY fip_inst $::dir]

    #Run solver
    set solve [exec /home/tmitchell/Documents/Floorplanner/TVLSI_floorplanner/ga_ns_floorplanner -g ./problem.txt -p /home/tmitchell/Documents/Floorplanner/TVLSI_floorplanner/ga_params.txt]

    #Run xdc generator and put xdc in correct place
    set xdc_out [exec python3 /home/tmitchell/Documents/xdc_gen/xdc.py $::dir/results.dat ./fie.srcs/constrs_1/imports/constraints/fie.xdc]

    refresh_design
}

set dir [pwd]
set floorplan [normalFloorPlan]
