# During isolating a design according to the IDF, cells that are not 
# apart of a module that is to be isolated need to be given a special
# property making them exempt from isolation. 

# This proc is used to extract any leaf cells that are not in the pblocks
# listed in the modules.txt file. 

# Leaf cells are saved into leaf_cells.txt to be used by the xdcIDF.py script

proc extract_leaf_cells_idf { } {
  set module_file $::dir
  append module_file "/modules.txt"
  set fp [open $module_file r]
  set file_data [read $fp]

  set call "get_cells -hier -filter {PRIMITIVE_LEVEL =~\"LEAF\" && "
  
  set i 0

  foreach module $file_data {
    if { $i==0 } {
      set i 1
    } else {
      append call " && "
    }
    set s "NAME !~ \"$module/*\"" 
    append call $s

  }
  append call "}"
  set len [string length $call]
  incr len -1
  set call [string range $call 0 $len]
  set cells [eval $call]

  set leaf_file $::dir
  append leaf_file "/leaf_cells.txt"
  set leaves [open $leaf_file w]

  # Save the cells to a cell file
  foreach cell $cells {
    puts $leaves $cell
  }
  close $leaves
}
