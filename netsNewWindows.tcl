#Extract the nets from the design


namespace eval netlist {
	# Determines the depth of the hierarchy at which 
	# module A and module B diverge. This is where nets
	# need to be mapped to
    proc hierarchydiff {moduleA moduleB} {
	set hierarchy_a [split $moduleA "/"]
	set hierarchy_b [split $moduleB "/"]
	set size_a [llength $hierarchy_a]
	set size_b [llength $hierarchy_b]
        set i 0
        set depth 1
        if {$size_a == 1 ^ $size_b ==1} {
            #If one list has a depth of one then they will always end up differing at one
            #as one module cannot be in another module
            return $depth
        }
		while {$i != $size_a} {
    	    set a [lindex $hierarchy_a $i]
    	    set b [lindex $hierarchy_b $i]
    	    if [string equal $a $b] {
    	    	#then the differ deeper in the hierarchy
    	    	incr i
                incr depth
    	    } else {
    	    	#then this is the depth that they differ
    	    	return $depth
    	    }
    	}
    	return $depth
	}
    #Maps the nets of module A to a depth d
    proc mapnets {module desiredDepth direction} {
        set cellFile ""
        set cellFile $::dir
        append cellFile "/"
        set replaced_name [string map {/ -} $module]
        append cellFile $replaced_name
        set fileDepth $desiredDepth
        append cellFile $fileDepth
        append cellFile $direction
        append cellFile ".txt" 
        if [file exists $cellFile] {
        #Save time and extract information from file
            #puts "using old value"
            set fp [open $cellFile r]
            set nets [read $fp]
            #puts $nets
        } else {
            set depth [netlist::getDepth $module]
            if {$depth == $desiredDepth} {
                #Then just need to get the nets at this level
                set cells [netlist::getCells $module $direction]
                set nets [netlist::getNets $cells]
            } else {
                set currDepth $depth
                set newName [netlist::createNewName $module $currDepth]
                while {$currDepth != $desiredDepth} {
                    #puts "start of while loop"
                    set cells [netlist::getCells $newName $direction]
                    set nets [netlist::getNets $cells]
                    set netNames [netlist::extractNetNames $nets]
                    set currDepth [incr currDepth -1]
                    set newName [netlist::createNewName $newName $currDepth]
                    set cells [netlist::getCells $newName $direction]
                    set cells [netlist::filterCells $cells $netNames]
                    netlist::saveNets $module $currDepth $direction $cells
                }
                set nets [netlist::getNets $cells]
            } 
            #Save the net file to be used again later
            set cellFile ""
            set cellFile $::dir
            append cellFile "/"
            set replaced_name [string map {/ -} $module]
            append cellFile $replaced_name
            set fileDepth $desiredDepth
            append cellFile $fileDepth
            append cellFile $direction
            append cellFile ".txt" 
            set fp [open $cellFile w]
            puts $fp $nets 
            close $fp 
            #puts "Saved net file"
        }
        return $nets
    }

    proc saveNets {module depth direction cells} {
        set nets [netlist::getNets $cells]
        set cellFile ""
        set cellFile $::dir
        append cellFile "/"
        set replaced_name [string map {/ -} $module]
        append cellFile $replaced_name
        set fileDepth $depth
        append cellFile $fileDepth
        append cellFile $direction
        append cellFile ".txt" 
        set fp [open $cellFile w]
        puts $fp $nets 
        close $fp 
        #puts "Saved net file"
    }


    
    #Counts the total number of nets in a list
    proc numOfNets {netsA netsB} {
        set num 0
        foreach net1 $netsA {
            foreach net2 $netsB {
                if [string equal $net1 $net2] {
                    incr num
                }
            }
        }
        return $num
    }

    proc getCells {module dir} {
        set cells [get_pins -of [get_cells $module] -filter "DIRECTION == $dir"]
        return $cells
    }

    # Returns the nets for a given module given the modules cell pins
    proc getNets {cells} {
        set nets [get_nets -of $cells]
        return $nets
    }
    proc getNetsFile {cells} {
        set nets [get_nets -of [get_cells -of [get_nets $cells]]]
        return $nets
    }
    #retuns the current module depth
    proc getDepth {module} {
        set hierarchy [split $module "/"]
        set size [llength $hierarchy]
        #The size of the list is the depth of the hierarchy
        return $size
    }
    #extracts the final portion of the name in the name structure
    #e.g. xspace_inst/node_3/sr_0/net will return net
    proc extractNetNames {l} {
        set output [list]
        foreach data  $l {
            set names [split $data "/"]
            set name_size [llength $names]
            set index [expr {$name_size -1}]
            lappend output [lindex $names $index]
        }
        return $output
    }

    proc createNewName {name depth} {
        set names [split $name "/"] 
        set s ""
        set i 0
        foreach data $names {
            if {$i < $depth} {
                append s $data
                incr i
            }
            if {$i != $depth} {
                append s "/"
            }
        }
        return $s
    }

    #Filters a list of cells, leaving only the cells that correspond to net names
    proc filterCells {cells netNames} {
        set result $cells
        set index 0
        foreach cell $cells {
            set keep "false"

            foreach net $netNames {
                set hierarchy [split $cell "/"]
                set length [expr {[llength $hierarchy] -1}]
                set cellName [lindex $hierarchy $length]
                if [string equal $cellName $net] {
                    set keep "true"
                    break
                }
            }
            if [string equal $keep "false"] {
                set result [lreplace $result $index $index]
            } else {
                incr index
            }
            
        }
        return $result
    }
}

	

#Read in module names
#set moduleFile $::dir
#append moduleFile "/modules.txt"

#create output file folder
set netDir $::dir
append netDir "/netmaps"
if [file isdirectory $netDir] {

} else {
    #set mkdir [exec mkdir $netDir]
}


set fp [open "./modules.txt" r]
#set fp [open $moduleFile r]
set file_data [read $fp]
close $fp

#split the data
set data [split $file_data "\n"]
#actual netlist extraction algorithm

#open output file
set connectFile $::dir
append connectFile "/connections.txt"
set connections [open $connectFile w]

#needed for 2017.2
set empty ""

set startTime [clock seconds]

foreach module_name1 $data {
    foreach module_name2 $data {
    	if [string equal $module_name1 $module_name2] {
    		# do nothing as module names refer to same module
    	} elseif [string match $module_name1 $empty] {
            # do nothing as 2017.2 picks up a "" from the list
        } elseif [string match $module_name2 $empty] {
            # do nothing as 2017.2 picks up a "" from the list

        } else {
    	    set s ""
    	    append s $module_name1
    	    append s " "
            append s $module_name2
            append s " "
            #puts "Starting net calc for $s"
    	    set depth [netlist::hierarchydiff $module_name1 $module_name2]
            #puts "$module_name1 and $module_name2 differ at depth $depth"
            set netsA [netlist::mapnets $module_name1 $depth "OUT"]
            #puts "Mapping $module_name2 to depth $depth IN"
            set netsB [netlist::mapnets $module_name2 $depth "IN"]
            #puts "Calculating # of connecting nets for $module_name1 $module_name2"
            set nets [netlist::numOfNets $netsA $netsB]
            #puts $nets
            append s $nets
            puts $connections $s
    	}
    }
}
set endTime [clock seconds]
puts "Start time: [clock format $startTime -format %H:%M:%S]"
puts "End time: [clock format $endTime -format %H:%M:%S]"

close $connections

	
