#Need save the initial hierarchy of the design

set linesFile $::dir
append linesFile "/lines.txt" #Utilization report is stored in this file

report_utilization -hierarchical -file $linesFile


set fp [open $linesFile r]
set file_data [read $fp]

close $fp


set moduleUtil $::dir
append moduleUtil "/modules.txt" ;#Module to be floorplanned are named in this file

#Extract module names stored in text file
set fp [open $moduleUtil r]
set names_data [read $fp]
close $fp

set hierarchyFile $::dir
append hierarchyFile "/hierarchical.txt" ;#Hierarchy file holds utilization info for each module

set hierarchical [open $hierarchyFile w]
set data [split $file_data "\n"]
set names [split $names_data "\n"]
foreach name $names {
    set parts [split $name "/"] ;#names lower in the hierarchy need to be mapped by each name section in the utilization information
    set index 0
   # puts $parts
    foreach line $data {
        set s "* "
        append s [lindex $parts $index]
        append s " *"
        if [string match $s $line] {
            incr index
            if { $index == [llength $parts] } {
                puts $hierarchical $line ;#Save the module information 
            } 
        }
    }
}
close $hierarchical

#From the saved file, extract lines based of | as these are used to separate each values
set modules [open $hierarchyFile r]
set module_data [read $modules]
#To save results to

set utilFile $::dir
append utilFile "/utilization.txt"
set utilization [open $utilFile w]

set data [split $module_data "|"]
set i 0

set s ""
#For each module this extracts information based on what resrouce types are wanted
foreach line $data {
    if {$i == 2} {
    	append s [string trim $line] ;#This holds the name of the module
    }
    if {$i == 3} { ;#This holds slice lUTS
        set l [string trim $line]
        set s1 [expr $l]
        #Rounding the number of slices up
        if { ($s1%4) != 0} {
        	set s1 [expr $s1+4]
        }
    }
    if {$i == 7} { ;#Holds slice ff
        set l [string trim $line] 
        set s2 [expr $l]
        #Rounding the number of ff up
        if { ($s2%8) != 0} {
        	set s2 [expr $s2+8]
        }
        set slice [expr max($s1/4,$s2/8)]
        append s " "
        append s $slice
    }
    if {$i == 8} {
    	#This has BRAM36
        set l [string trim $line]
        set bram36 [expr $l]
    }

    if {$i == 9} {
        #This deals with BRAM18
        #add one so total bram is rounded up
        set l [string trim $line]
        set bram18 [expr $l]
        #have to divide BRAM18 by 2
        set bram36 [expr ($bram36 * 2)]
        set bram [expr ($bram18 + $bram36)]

        append s " "
        append s $bram
    }

    if {$i == 10} {
        #This value is the number of DSP48 blocks
        append s " "
        append s [string trim $line]
    }

    incr i
    if {$i == 11} {
    	puts $utilization $s ;#Save create line to the utilization file
    	set i 0
        set s ""
    }
}


close $modules
close $utilization

#Delete the lines and hierarchical files
#file delete -force "lines.txt"
#file delete -force "hierarchical.txt"
